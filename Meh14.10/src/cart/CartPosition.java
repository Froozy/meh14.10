package cart;

public class CartPosition {
	String product;
	private int count;
	double price;
	double fullprice;
	 
	public CartPosition(Product x,int amount)
	{
		product = x.name;
		count = amount;
		price = x.prize;
		fullprice = price*count;
	}
	public double getFullprice() {
		return fullprice;
	}

	public void setFullprice(double fullprice) {
		this.fullprice = fullprice;
	}
	
	public String getProduct() 
	{
		return product;
	}
	public void setProduct(String product) 
	{
		this.product = product;
	}
	public double getPrice() 
	{
		return price;
	}
	public void setPrice(double price) 
	{
		this.price = price;
	}
	public int getCount() 
	{
		return count;
	}
	public void setCount(int count) 
	{
		this.count = count;
	}


}
