package Discounts;

import java.util.ArrayList;
import java.util.List;

import cart.CartPosition;


public class SevenPlusOne implements Discount {
	List<CartPosition> List = new ArrayList<CartPosition>();
	
	public SevenPlusOne(List<CartPosition> LIST)
	{
		List = LIST;
	}
	
	@Override
	public void setCartPosition(CartPosition x) {
		List.add(x);
	}
	public void ChangeList(List<CartPosition> x)
	{
		x = List;
	}

	@Override
	public void discount() {
		for(CartPosition x : List)		// Po liscie
		{
			if(x.getCount()>= 7)
			{
				 int temp = x.getCount()/7;
				 double temp2 = x.getFullprice() - x.getPrice()*temp;
				 x.setFullprice(temp2);
				 
			}
		}
		
	}

}