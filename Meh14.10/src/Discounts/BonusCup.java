package Discounts;

import java.util.ArrayList;
import java.util.List;

import cart.Cart;
import cart.CartPosition;
import cart.Product;

public class BonusCup implements Discount {
	List<CartPosition> List = new ArrayList<CartPosition>();
	
	public BonusCup(List<CartPosition> list2) {
		List = list2;
	}

	@Override
	public void setCartPosition(CartPosition x) {
		List.add(x);	
	}

	@Override
	public void discount() {
		if(List.size() >= 3)
		{
			Product Cup = new Product("Cup", 0);
			CartPosition cup = new CartPosition(Cup, 1);
			List.add(cup);
		}
		
	}

	@Override
	public void ChangeList(java.util.List<CartPosition> x) {
		x = List;
	}

}
