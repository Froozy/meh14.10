import Discounts.BonusCup;
import Discounts.SevenPlusOne;
import cart.Cart;
import cart.CartPosition;
import cart.Product;


public class Test {

	public static void main(String[] args) {
		Cart Tada = new Cart();
		Product Onion = new Product("Onion",1);
		Product Penguin = new Product("Penguin",400);
		Product Table = new Product("Table",239.99);
		CartPosition One = new CartPosition(Onion,40);
		CartPosition Two = new CartPosition(Penguin,3);
		CartPosition Three = new CartPosition(Table,1);
		Tada.list.add(One);
		Tada.list.add(Two);
		Tada.list.add(Three);
		SevenPlusOne seven = new SevenPlusOne(Tada.list);
		seven.ChangeList(Tada.list);
		BonusCup Cuup = new BonusCup(Tada.list);
		Cuup.ChangeList(Tada.list);
		seven.discount();
		Cuup.discount();
		Tada.ReadAll();
	}

}
